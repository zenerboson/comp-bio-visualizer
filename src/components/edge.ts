import * as paper from 'paper';
import { ReactantNode, ReactionNode } from './node';

export class Edge {
  public connection: ReactantNode | ReactionNode | undefined;
  public mode: boolean;
  private kmode: boolean;
  private iso: [boolean, boolean];
  private path: paper.Path;
  private tool: paper.Tool;
  private extendListener: any;
  private finalizeListener: any;
  private is: boolean;
  private destroyed: boolean;
  private remove: (edge: Edge | ReactionNode) => void;
  private connectHandler: (node: HTMLElement) => ReactantNode;
  private origin: ReactantNode | ReactionNode;

  constructor(
    point: paper.Point | ReactionNode | ReactantNode,
    origin: ReactantNode | ReactionNode,
    remove: (edge: Edge) => void,
    mode?: boolean,
    kmode?: boolean
  ) {
    if ((point as any).isReaction) {
      this.iso = [true, false];
    } else {
      this.iso = [true, true];
    }
    this.kmode = kmode;
    this.path = new paper.Path();
    this.path.strokeColor = '#ff6f00';
    if (mode) {
      this.path.strokeColor = '#2196f3';
    }
    this.path.strokeCap = 'round';
    this.path.strokeJoin = 'round';
    this.is = false;
    this.mode = mode || false;
    this.path.strokeWidth = 10;
    if ((point as any).x) {
      point = point as paper.Point;
      this.path.add(point);
      this.path.add(point);
      this.path.add(point);
    } else {
      const r = origin.getElement().getBoundingClientRect();
      const p = new paper.Point(r.left + r.width / 2, r.bottom - r.height / 2);
      this.path.add(p);
      this.path.add(p);
      this.path.add(p);
      this.is = true;
      this.connection = point as ReactionNode | ReactantNode;
    }
    this.origin = origin;
    this.remove = remove;
    this.tool = new paper.Tool();
    this.extendListener = this.extend.bind(this);
    this.finalizeListener = this.finalize.bind(this);
    if (!(point as any).x) {
      setTimeout(() => {
        this.finalize({ path: [this.connection.getOutputMarker()] }, true);
      }, 300);
    } else {
      document.addEventListener('mousemove', this.extendListener);
      document.addEventListener('mouseup', this.finalizeListener);
    }
  }
  public getStart() {
    return this.origin;
  }
  public getEnd() {
    return this.connection;
  }
  public onConnect(
    handler: (node: HTMLElement) => ReactantNode | ReactionNode
  ) {
    this.connectHandler = handler as (node: HTMLElement) => ReactantNode;
  }
  public destroy(stop?: boolean) {
    if (this.destroyed) {
      return;
    }
    this.destroyed = true;
    if ((this.origin as ReactionNode).isReaction) {
      (this.origin as ReactionNode).removeEdge(this);
    }
    if (this.connection && (this.connection as ReactionNode).isReaction) {
      (this.connection as ReactionNode).removeEdge(this);
    }
    this.path.remove();
    this.remove(this);
  }
  public update() {
    let rect;
    if (this.is) {
      if (!this.iso[0]) {
        rect = this.origin.getOutputMarker().getBoundingClientRect();
      } else {
        rect = this.origin.getElement().getBoundingClientRect();
      }
    } else {
      rect = this.origin.getOutputMarker().getBoundingClientRect();
    }
    this.path.segments[0].point = paper.view.viewToProject(
      new paper.Point(rect.left + rect.width / 2, rect.bottom - rect.height / 2)
    );
    if (this.mode) {
      if (this.kmode) {
        rect = (this.connection as any)
          .getOutputMarker()
          .getBoundingClientRect();
      } else {
        rect = (this.connection as any)
          .getInputMarker()
          .getBoundingClientRect();
      }
    } else {
      if ((this.connection as ReactionNode).isReaction && this.iso[1]) {
        rect = (this.connection as any).getElement().getBoundingClientRect();
      } else {
        rect = (this.connection as any)
          .getOutputMarker()
          .getBoundingClientRect();
      }
    }
    this._extend(
      paper.view.viewToProject(
        new paper.Point(
          rect.left + rect.width / 2,
          rect.bottom - rect.height / 2
        )
      )
    );
  }
  private extend(event: MouseEvent) {
    const target = (event as any).path[0] as HTMLElement;
    if (target.classList.contains('input') || this.kmode) {
      this.path.strokeColor = '#2196f3';
      this.mode = true;
    } else {
      if (this.mode) {
        if (this.path.lastCurve) {
          this.path.lastCurve.remove();
        }
      }
      if (!this.kmode) {
        this.mode = false;
        this.path.strokeColor = '#ff6f00';
      }
    }
    this._extend(paper.view.viewToProject(new paper.Point(event.x, event.y)));
  }
  private _extend(point: paper.Point) {
    this.path.removeSegment(1);
    if (!this.mode) {
      this.path.removeSegment(1);
      if (point.y < this.path.segments[0].point.y) {
        this.path.add(new paper.Point(point.x, this.path.segments[0].point.y));
      } else {
        this.path.add(new paper.Point(this.path.segments[0].point.x, point.y));
      }
      this.path.add(point);
    } else {
      this.path.removeSegment(1);
      if (point.y > this.path.segments[0].point.y) {
        this.path.add(new paper.Point(point.x, this.path.segments[0].point.y));
      } else {
        this.path.add(new paper.Point(this.path.segments[0].point.x, point.y));
      }
      this.path.add(point);
    }
  }
  private finalize(
    event: { path: HTMLElement[] },
    k?: boolean
  ): boolean | void {
    let target = event.path[0] as HTMLElement;
    if (
      target.classList[0] !== 'output' &&
      target.classList[0] !== 'input' &&
      target.classList[0] !== 'reaction' &&
      target.classList[0] !== 'inner'
    ) {
      this.path.remove();
      this.remove(this);
    } else {
      let n = false;
      if (target.classList[0] === 'inner') {
        target = target.parentElement;
      }
      if (target.classList[0] === 'reaction') {
        n = true;
      }
      const rect = target.getBoundingClientRect();
      this._extend(
        paper.view.viewToProject(
          new paper.Point(
            rect.left + rect.width / 2,
            rect.bottom - rect.height / 2
          )
        )
      );
      if (this.connectHandler) {
        this.connection = this.connectHandler(target);
        if (this.origin.getElement().isSameNode(this.connection.getElement())) {
          this.remove(this);
          this.destroy();
          return;
        }
        if (!this.mode && !this.is && !n) {
          const reaction = new ReactionNode(
            this.getFalseMidpoint(),
            this.origin,
            this.connection
          );
          reaction.update();
          reaction.addSEdge(this);
          this.remove(reaction);
          reaction.onEdge((this.origin as ReactantNode).callback);
          this.connection = reaction;
        }
      } else {
        this.update();
      }
    }
    document.removeEventListener('mousemove', this.extendListener);
    document.removeEventListener('mouseup', this.finalizeListener);
  }
  private getFalseMidpoint() {
    const p1 = this.path.segments[0].point;
    const p2 = this.path.segments[1].point;
    const p3 = this.path.segments[2].point;
    const l1 = Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
    const l2 = Math.sqrt(Math.pow(p3.x - p2.x, 2) + Math.pow(p3.y - p2.y, 2));
    if (l1 > l2) {
      return paper.view.projectToView(
        new paper.Point(p1.x + (p2.x - p1.x) / 2, p1.y + (p2.y - p1.y) / 2)
      );
    } else {
      return paper.view.projectToView(
        new paper.Point(p2.x + (p3.x - p2.x) / 2, p2.y + (p3.y - p2.y) / 2)
      );
    }
  }
}
