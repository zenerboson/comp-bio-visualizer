import * as paper from 'paper';

import { Edge } from './edge';

function ro() {
  return (([1e7] as any) + -1e3 + -4e3 + -8e3 + -1e11).replace(
    /[018]/g,
    (c: any) =>
      // tslint:disable:no-bitwise
      (
        c ^
        ((crypto.getRandomValues(new Uint8Array(1) as any) as any)[0] &
          (15 >> (c / 4)))
      ).toString(16)
  );
}

const DPR = window.devicePixelRatio;

export class ReactantNode {
  public callback: (point: paper.Point, mode?: boolean) => Promise<Edge | void>;
  private element: HTMLElement;
  private edges: Edge[];
  private removeCall: (node: ReactantNode) => void;
  private position: { x: number; y: number };
  private placed: boolean;
  private output: HTMLElement | undefined;
  private edgeCallback: (point: paper.Point, mode?: boolean) => void;

  constructor(symbol: string, remove: (node: ReactantNode) => void) {
    this.removeCall = remove;
    this.element = document.createElement('div');
    this.element.classList.add('label');
    const position = new paper.Point(60, 60);
    this.element.style.left = `${position.x - 15 * DPR}px`;
    this.element.style.top = `${position.y - 15 * DPR}px`;
    this.position = { x: position.x, y: position.y };
    this.element.addEventListener('mousedown', this.beginMove.bind(this));
    this.element.setAttribute('data-identifier', `reactant-${ro()}`);
    this.element.textContent = symbol;
    this.placed = false;
    this.edges = [];
    document.body.appendChild(this.element);
  }
  public getElement() {
    return this.element;
  }
  public getOutputMarker() {
    return this.output;
  }
  public onEdge(
    callback: (
      point: paper.Point,
      mode?: boolean,
      reaction?: ReactionNode
    ) => Promise<Edge | void>
  ) {
    this.callback = callback;
    this.edgeCallback = (v: paper.Point) => {
      callback(v).then(edge => {
        if (edge) {
          this.edges.push(edge!);
        }
      });
    };
    if (this.output) {
      this.output.addEventListener('mousedown', () => {
        const r = this.output.getBoundingClientRect();
        let center = new paper.Point(
          r.left + r.width / 2,
          r.bottom + r.height / 2
        );
        center = paper.view.viewToProject(center);
        this.edgeCallback(center);
      });
    }
  }
  public addEdge(edge: Edge) {
    this.edges.push(edge);
  }
  public beginMove(event: MouseEvent) {
    if (((event as any).path[0] as HTMLElement).isSameNode(this.output)) {
      return;
    }
    const handler = this.move.bind(this);
    document.addEventListener('mousemove', handler);
    const c = document.querySelector('.container');
    document.body.setAttribute(
      'data-expanded-id',
      this.element.getAttribute('data-identifier')
    );
    c.classList.remove('eo');
    this.element.classList.remove('dropped');
    const unregister = () => {
      document.querySelector('.title').textContent = this.element.textContent;
      document.querySelector('.subtitle').textContent = 'reactant node';
      document.querySelector('.action').textContent = 'remove';
      c.classList.remove('react');
      if (c.classList.contains('open')) {
        c.classList.add('eo');
      } else {
        c.classList.add('open');
        c.classList.add('eo');
      }
      document.removeEventListener('mousemove', handler);
      document.removeEventListener('mouseup', unregister);
      this.element.classList.add('dropped');
      if (!this.placed) {
        this.place();
      }
    };
    document.addEventListener('mouseup', unregister);
  }
  public translate(x: number, y: number) {
    this.position.x += x;
    this.position.y += y;
    this.element.style.left = `${this.position.x - 15 * DPR}px`;
    this.element.style.top = `${this.position.y - 15 * DPR}px`;
  }
  public remove() {
    this.edges.forEach(edge => edge.destroy());
    this.element.classList.add('destroy');
    setTimeout(() => {
      this.element.remove();
      if (this.output) {
        this.output.remove();
      }
    }, 300);
  }
  public move(event: any) {
    document.querySelector('.container').classList.remove('open');
    this.position = {
      x: this.position.x + event.movementX,
      y: this.position.y + event.movementY
    };
    this.element.style.left = `${this.position.x - 15 * DPR}px`;
    this.element.style.top = `${this.position.y - 15 * DPR}px`;
    if (event.movementX !== 0 || event.movementY !== 0) {
      this.edges.forEach(edge => edge.update());
    }
  }
  private place() {
    this.element.classList.add('placed');
    this.element.classList.add('placing');
    this.element.classList.add('dropped');
    this.placed = true;
    const ring = new paper.Path.Circle(
      paper.view.viewToProject(
        new paper.Point(this.position.x, this.position.y)
      ),
      15 * DPR
    );
    ring.strokeColor = new paper.Color('#ff8f00');
    ring.strokeWidth = 5;
    const rect = this.element.getBoundingClientRect();
    if (0 < rect.left && rect.left < 90 && 0 < rect.top && rect.top < 90) {
      this.removeCall(this);
      ring.remove();
      document.querySelector('.container').classList.remove('open');
    }
    const tween = (ring as any)
      .tween(
        { strokeWidth: 0 },
        { strokeWidth: 15 },
        { duration: 500, easing: 'easeInOutQuint' }
      )
      .then(() => {
        this.addOutputMarker().then((elem: HTMLElement) => {
          this.output = elem;
          this.output.addEventListener('mousedown', () => {
            const r = this.output.getBoundingClientRect();
            let center = new paper.Point(
              r.left + r.width / 2,
              r.bottom - r.height / 2
            );
            center = paper.view.viewToProject(center);
            this.edgeCallback(center);
          });
          this.element.classList.remove('placing');
          ring.remove();
        });
      });
    tween.on('update', (update: any) => {
      const newColor = ring.strokeColor as paper.Color;
      newColor.alpha = 1 - update.progress;
      ring.strokeColor = newColor;
      ring.scale(1.015);
    });
  }
  private addOutputMarker(): Promise<HTMLElement> {
    return new Promise((resolve, reject) => {
      const elem = document.createElement('div');
      elem.classList.add('output');
      this.element.appendChild(elem);
      resolve(elem);
    });
  }
}

export class ReactionNode {
  public origin: ReactantNode | ReactionNode;
  public connection: ReactantNode | ReactionNode;
  public callback: (
    point: paper.Point,
    mode?: boolean | string
  ) => Promise<Edge | void>;
  public isRotated: boolean;
  public offset: { x: number; y: number };
  public isReaction: boolean;
  private element: HTMLElement;
  private position: { x: number; y: number };
  private output: HTMLElement;
  private edgeCallback: (point: paper.Point, mode?: boolean | string) => void;
  private input: HTMLElement;
  private edges: Edge[];
  private sEdges: Edge[];

  constructor(
    position: paper.Point,
    origin: ReactantNode | ReactionNode,
    connection: ReactantNode | ReactionNode
  ) {
    this.element = document.createElement('div');
    this.element.classList.add('reaction');
    this.element.setAttribute('data-identifier', `reaction-${ro()}`);
    this.element.setAttribute('reaction', '1');
    this.element.style.left = `${position.x - 15 * DPR}px`;
    this.element.style.top = `${position.y - 15 * DPR}px`;
    this.element.addEventListener('mousedown', this.beginMove.bind(this));
    document.body.setAttribute(
      'data-expanded-id',
      this.element.getAttribute('data-identifier')
    );
    this.position = { x: position.x, y: position.y };
    this.offset = { x: 0, y: 0 };
    this.origin = origin;
    this.connection = connection;
    this.element.innerHTML = `<div class="inner">+</div>`;
    this.addInputMarker().then(elem => {
      this.input = elem;
      this.input.addEventListener('mousedown', () => {
        const r = this.input.getBoundingClientRect();
        let center = new paper.Point(
          r.left + r.width / 2,
          r.bottom - r.height / 2
        );
        center = paper.view.viewToProject(center);
        this.edgeCallback(center, '1');
      });
    });
    this.addOutputMarker().then(elem => {
      this.output = elem;
      this.output.addEventListener('mousedown', () => {
        const r = this.output.getBoundingClientRect();
        let center = new paper.Point(
          r.left + r.width / 2,
          r.bottom - r.height / 2
        );
        center = paper.view.viewToProject(center);
        this.edgeCallback(center);
      });
    });
    this.edges = [];
    this.sEdges = [];
    this.isReaction = true;
    document.body.appendChild(this.element);
    const c = document.querySelector('.container');
    c.classList.remove('eo');
    setTimeout(() => {
      this.updateTitle();
      document.querySelector('.subtitle').textContent = 'reaction node';
      document.querySelector('.action').textContent = 'remove';
      c.classList.add('react');
      if (c.classList.contains('open')) {
        c.classList.add('eo');
      } else {
        c.classList.add('open');
        c.classList.add('eo');
      }
    }, 5);
  }
  public destroy() {
    this.sEdges.forEach(edge => edge.destroy(false));
    this.edges.forEach(edge => edge.destroy(false));
    this.edges = [];
    this.sEdges = [];
    this.element.classList.add('destroy');
    setTimeout(() => {
      this.element.remove();
      this.input.remove();
      this.output.remove();
    }, 300);
  }
  public update() {
    this.edges.forEach(edge => edge.update());
    this.move({ movementX: 0, movementY: 0 });
  }
  public setPosition(position: paper.Point) {
    this.element.style.left = `${position.x - 15 * DPR}px`;
    this.element.style.top = `${position.y - 15 * DPR}px`;
    this.position = { x: position.x, y: position.y };
  }
  public translate(x: number, y: number) {
    this.position.x += x;
    this.position.y += y;
    this.element.style.left = `${this.position.x - 15 * DPR}px`;
    this.element.style.top = `${this.position.y - 15 * DPR}px`;
  }
  public getElement() {
    return this.element;
  }
  public addEdge(edge: Edge) {
    this.edges.push(edge);
  }
  public addSEdge(edge: Edge) {
    this.sEdges.push(edge);
  }
  public getOutputMarker() {
    return this.output;
  }
  public getInputMarker() {
    return this.input;
  }
  public rotate(b: boolean) {
    this.offset = { x: 0, y: 0 };
    this.isRotated = b;
    if (b) {
      this.element.classList.add('rotate');
    } else {
      this.element.classList.remove('rotate');
    }
  }
  public getProduct() {
    const sk = this.sEdges.map(edge => {
      if (
        edge
          .getEnd()
          .getElement()
          .isSameNode(this.element)
      ) {
        const e1 = edge.getStart();
        if ((e1 as ReactionNode).isReaction) {
          return (e1 as ReactionNode).getRawProduct();
        } else {
          return e1.getElement().textContent;
        }
      } else {
        const e2 = edge.getEnd();
        if ((e2 as ReactionNode).isReaction) {
          return (e2 as ReactionNode).getRawProduct();
        } else {
          return e2.getElement().textContent;
        }
      }
    });
    let st: string[] = sk
      .join('')
      .split('')
      .sort();
    const sm = new Map<string, number>();
    st.forEach(c => {
      if (!sm.has(c)) {
        sm.set(c, 1);
      } else {
        sm.set(c, sm.get(c) + 1);
      }
    });
    st = [];
    sm.forEach((v, k) => {
      if (v > 1) {
        st.push(`<span class='product' data-count=${v}>${k}</span>`);
      } else {
        st.push(`<span class='product'>${k}</span>`);
      }
    });
    return st.join('');
  }
  public getRawProduct() {
    const sk = this.sEdges.map(edge => {
      if (
        edge.getEnd() &&
        edge
          .getEnd()
          .getElement()
          .isSameNode(this.element)
      ) {
        const e1 = edge.getStart();
        if ((e1 as ReactionNode).isReaction) {
          return (e1 as ReactionNode).getRawProduct();
        } else {
          return e1.getElement().textContent;
        }
      } else {
        const e2 = edge.getEnd();
        if (!e2) {
          return;
        }
        if ((e2 as ReactionNode).isReaction) {
          return (e2 as ReactionNode).getRawProduct();
        } else {
          return e2.getElement().textContent;
        }
      }
    });
    const st: string[] = sk
      .join('')
      .split('')
      .sort();
    return st.join('');
  }
  public beginMove(event: MouseEvent) {
    if (
      ((event as any).path[0] as HTMLElement).isSameNode(this.output) ||
      ((event as any).path[0] as HTMLElement).isSameNode(this.input)
    ) {
      return;
    }
    const handler = this.move.bind(this);
    document.body.setAttribute(
      'data-expanded-id',
      this.element.getAttribute('data-identifier')
    );
    document.addEventListener('mousemove', handler);
    const c = document.querySelector('.container');
    c.classList.remove('eo');
    this.element.classList.remove('dropped');
    const unregister = () => {
      this.updateTitle();
      document.querySelector('.subtitle').textContent = 'reaction node';
      document.querySelector('.action').textContent = 'remove';
      c.classList.add('react');
      if (c.classList.contains('open')) {
        c.classList.add('eo');
      } else {
        c.classList.add('open');
        c.classList.add('eo');
      }
      document.removeEventListener('mousemove', handler);
      document.removeEventListener('mouseup', unregister);
      this.element.classList.add('dropped');
    };
    document.addEventListener('mouseup', unregister);
  }
  public removeEdge(r: Edge) {
    this.edges = this.edges.filter(edge => {
      if (r === edge) {
        edge.destroy(true);
      }
      return r !== edge;
    });
    this.sEdges = this.sEdges.filter(edge => {
      if (r === edge) {
        edge.destroy(true);
      }
      return r !== edge;
    });
    if (this.sEdges.length < 2) {
      this.destroy();
    }
  }
  public onEdge(
    callback: (
      point: paper.Point,
      mode?: boolean | string,
      reaction?: ReactionNode
    ) => Promise<Edge | void>
  ) {
    this.callback = callback;
    this.edgeCallback = (v: paper.Point, b?: boolean | string) => {
      callback(v, b, this).then(edge => {
        if (edge) {
          this.edges.push(edge!);
        }
      });
    };
    this.callback(new paper.Point(0, 0), 'update');
    if (this.input) {
      this.input.addEventListener('mousedown', () => {
        const r = this.input.getBoundingClientRect();
        let center = new paper.Point(
          r.left + r.width / 2,
          r.bottom + r.height / 2
        );
        center = paper.view.viewToProject(center);
        this.edgeCallback(center, true);
      });
    }
  }
  public move(event: any) {
    document.querySelector('.container').classList.remove('open');
    this.position.x += event.movementX;
    this.position.y += event.movementY;
    this.element.style.left = `${this.position.x - 15 * DPR}px`;
    this.element.style.top = `${this.position.y - 15 * DPR}px`;
    if (event.movementX !== 0 || event.movementY !== 0) {
      this.edges.forEach(edge => edge.update());
      this.sEdges.forEach(edge => edge.update());
    }
  }
  private updateTitle() {
    const e1 = this.origin as ReactionNode;
    let ks = this.sEdges.map(edge => {
      if (
        edge
          .getEnd()
          .getElement()
          .isSameNode(this.element)
      ) {
        const e3 = edge.getStart();
        if ((e3 as ReactionNode).isReaction) {
          return (e3 as ReactionNode).getRawProduct();
        } else {
          return e3.getElement().textContent;
        }
      } else {
        const e4 = edge.getEnd();
        if ((e4 as ReactionNode).isReaction) {
          return (e4 as ReactionNode).getRawProduct();
        } else {
          return e4.getElement().textContent;
        }
      }
    });
    ks = ks.map(s => {
      return `<span class='plus'>+</span><div class='oproduct nosp'>${s}</div>`;
    });
    const km = ks.join('').slice(27);
    const p1 = e1.isReaction
      ? e1.getProduct()
      : `<span class='product nosp'>${e1.getElement().textContent}</span>`;
    const e2 = this.connection as ReactionNode;
    const p2 = e2.isReaction
      ? e2.getProduct()
      : `<span class='product nosp'>${e2.getElement().textContent}</span>`;
    document.querySelector(
      '.title'
    ).innerHTML = `${km}<span class='arrow'>→</span>
      <div class='oproduct'>${this.getProduct()}</div>`;
  }
  private addOutputMarker(): Promise<HTMLElement> {
    return new Promise((resolve, reject) => {
      const elem = document.createElement('div');
      elem.classList.add('output');
      this.element.appendChild(elem);
      resolve(elem);
    });
  }
  private addInputMarker(): Promise<HTMLElement> {
    return new Promise((resolve, reject) => {
      const elem = document.createElement('div');
      elem.classList.add('input');
      this.element.appendChild(elem);
      resolve(elem);
    });
  }
}
