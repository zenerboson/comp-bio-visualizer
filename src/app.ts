import * as paper from 'paper';

import { Edge } from './components/edge';
import { ReactantNode, ReactionNode } from './components/node';

function successor(str: string) {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const length = alphabet.length;
  let result = str;
  let i = str.length;

  while (i >= 0) {
    const last = str.charAt(--i);
    let next = '';
    let carry = false;

    if (isNaN(last as any)) {
      const index = alphabet.indexOf(last.toLowerCase());

      if (index === -1) {
        next = last;
        carry = true;
      } else {
        const isUpperCase = last === last.toUpperCase();
        next = alphabet.charAt((index + 1) % length);
        if (isUpperCase) {
          next = next.toUpperCase();
        }

        carry = index + 1 >= length;
        if (carry && i === 0) {
          const added = isUpperCase ? 'A' : 'a';
          result = added + next + result.slice(1);
          break;
        }
      }
    } else {
      let n = +last + 1;
      if (n > 9) {
        n = 0;
        carry = true;
      }

      if (carry && i === 0) {
        result = '1' + n + result.slice(1);
        break;
      }
    }

    result = result.slice(0, i) + next + result.slice(i + 1);
    if (!carry) {
      break;
    }
  }
  return result;
}

export class App {
  private edges: Edge[];
  private newMonomerPlaceholder: HTMLElement;
  private reactantNodes: ReactantNode[];
  private freeIdentifiers: string[];
  private reactionNodes: ReactionNode[];
  private lastchar: string;

  constructor(canvas: HTMLCanvasElement) {
    this.freeIdentifiers = [];
    this.lastchar = 'a';
    window.addEventListener('load', () => {
      document
        .querySelector('canvas')
        .addEventListener('mousedown', (e: any) => {
          container.classList.remove('open');
        });
      const container = document.querySelector('.container')!;
      document.querySelector('.close')!.addEventListener('click', () => {
        container.classList.remove('open');
      });
      document.querySelector('.action')!.addEventListener('click', () => {
        container.classList.remove('open');
        if (
          document.querySelector('.subtitle').textContent === 'reactant node'
        ) {
          const t = document.querySelector('.title').textContent;
          this.reactantNodes.forEach(node => {
            if (node.getElement().textContent === t) {
              this.freeIdentifiers.push(t);
              this.freeIdentifiers.sort();
              this.newMonomerPlaceholder.textContent = this.freeIdentifiers[0];
              node.remove();
              this.reactantNodes.splice(this.reactantNodes.indexOf(node), 1);
            }
          });
        } else if (
          document.querySelector('.subtitle').textContent === 'reaction node'
        ) {
          const el = document.querySelector(
            `[data-identifier="${document.body.getAttribute(
              'data-expanded-id'
            )}"]`
          );
          this.reactionNodes.forEach(node => {
            if (node.getElement().isSameNode(el)) {
              node.destroy();
              this.reactionNodes.splice(this.reactionNodes.indexOf(node), 1);
            }
          });
        }
      });
    });
    paper.setup(canvas);
    const placeholder = document.createElement('div');
    placeholder.classList.add('label');
    placeholder.classList.add('placeholder');
    document.body.appendChild(placeholder);
    placeholder.style.left = `${15 * window.devicePixelRatio}px`;
    placeholder.style.top = `${15 * window.devicePixelRatio}px`;
    placeholder.textContent = 'a';
    this.reactantNodes = [];
    this.reactionNodes = [];
    placeholder.addEventListener('mousedown', (event: MouseEvent) => {
      let node: ReactantNode;
      if (this.freeIdentifiers.length > 0) {
        this.newMonomerPlaceholder.textContent = this.freeIdentifiers[0];
        node = new ReactantNode(
          this.newMonomerPlaceholder.textContent,
          this.removeNode.bind(this)
        );
        if (
          [this.lastchar, this.newMonomerPlaceholder.textContent].sort()[1] !==
          this.lastchar
        ) {
          this.lastchar = this.newMonomerPlaceholder.textContent;
        }
        this.freeIdentifiers.splice(0, 1);
        if (this.freeIdentifiers.length > 0) {
          this.newMonomerPlaceholder.textContent = this.freeIdentifiers[0];
        } else {
          this.newMonomerPlaceholder.textContent = successor(this.lastchar);
        }
      } else {
        node = new ReactantNode(
          this.newMonomerPlaceholder.textContent,
          this.removeNode.bind(this)
        );
        if (
          [this.lastchar, this.newMonomerPlaceholder.textContent].sort()[1] !==
          this.lastchar
        ) {
          this.lastchar = this.newMonomerPlaceholder.textContent;
        }
        this.newMonomerPlaceholder.textContent = successor(
          this.newMonomerPlaceholder.textContent
        );
      }
      this.reactantNodes.push(node);
      const cb = this.newEdge.bind(this);
      node.onEdge(
        (v, mode?: boolean, reaction?: ReactionNode): Promise<void | Edge> => {
          return cb(node, v, mode, reaction);
        }
      );
      node.beginMove(event);
    });
    let mouseState = false;
    document.addEventListener('mousedown', e => {
      if ((e.target as HTMLElement).tagName === 'CANVAS') {
        mouseState = true;
      }
    });
    document.addEventListener('mouseup', () => {
      mouseState = false;
    });
    document.addEventListener('mousemove', e => {
      if (mouseState) {
        paper.view.translate(new paper.Point(e.movementX, e.movementY));
        this.reactantNodes.forEach(node =>
          node.translate(e.movementX, e.movementY)
        );
        this.reactionNodes.forEach(node =>
          node.translate(e.movementX, e.movementY)
        );
      }
    });
    this.newMonomerPlaceholder = placeholder;
    this.edges = [];
  }
  public removeNode(node: ReactantNode) {
    this.freeIdentifiers.push(node.getElement().textContent);
    this.freeIdentifiers.sort();
    this.newMonomerPlaceholder.textContent = this.freeIdentifiers[0];
    node.remove();
    this.reactantNodes.splice(this.reactantNodes.indexOf(node), 1);
  }
  private newEdge(
    node: ReactantNode | ReactionNode,
    point: paper.Point | ReactionNode | ReactantNode,
    mode?: boolean | string,
    rn?: ReactionNode
  ): Promise<Edge | null> {
    return new Promise((resolve, reject) => {
      if (mode === 'update') {
        this.edges.forEach(e => e.update);
        return;
      }
      if (rn) {
        node = rn;
      }
      let edge: Edge;
      if (mode === '1') {
        edge = new Edge(point, node, this.removeEdge.bind(this), true, true);
        if ((node as ReactionNode).isReaction) {
          (node as ReactionNode).addEdge(edge);
        }
      } else {
        edge = new Edge(point, node, this.removeEdge.bind(this), false);
        if ((node as ReactionNode).isReaction) {
          (node as ReactionNode).addSEdge(edge);
        }
      }
      edge.onConnect((cbNode: HTMLElement) => {
        if (cbNode.classList.contains('input')) {
          let iNode: ReactionNode | undefined;
          for (const reaction of this.reactionNodes) {
            if (reaction.getElement().isSameNode(cbNode.parentElement)) {
              iNode = reaction;
            }
          }
          if (!iNode) {
            edge.destroy();
            resolve(null);
            return;
          }
          iNode.addEdge(edge);
          resolve(edge);
          console.log(iNode);
          return iNode;
        }
        let innerNode: ReactantNode | undefined;
        let k = false;
        if (cbNode.classList.contains('reaction')) {
          cbNode = cbNode.firstChild as HTMLElement;
          k = true;
        }
        for (const reactant of this.reactantNodes) {
          if (reactant.getElement().isSameNode(cbNode.parentElement)) {
            innerNode = reactant;
            break;
          }
        }
        if (!innerNode) {
          for (const reaction of this.reactionNodes) {
            if (reaction.getElement().isSameNode(cbNode.parentElement)) {
              innerNode = reaction as any;
              break;
            }
          }
        }
        if (!innerNode) {
          edge.destroy();
          resolve(null);
          return;
        }
        if (k) {
          (innerNode as any).addSEdge(edge);
        } else {
          innerNode.addEdge(edge);
        }
        resolve(edge);
        setTimeout(() => {
          this.edges.forEach(e => e.update);
        }, 10);
        return innerNode;
      });
      this.edges.push(edge);
    });
  }
  private removeEdge(edge: Edge | ReactionNode) {
    if ((edge as any).isReaction) {
      edge = edge as ReactionNode;
      this.reactionNodes.push(edge);
      this.newEdge(edge, edge.connection);
      return;
    }
    edge = edge as Edge;
    const index = this.edges.indexOf(edge);
    if (index !== -1) {
      this.edges.splice(index, 1);
    }
  }
}
